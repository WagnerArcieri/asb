package br.com.uware.androidstudiobrasiljetpackcompose.components

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext


/**
 * ToastMessage
 *
 * @author Rodrigo Leutz
 * @version 1.0.0 - 30/05/2022 - Initial release.
 */
@Composable
fun ToastMessage(
    message: String,
    duration: Int = Toast.LENGTH_SHORT
) {
    val context = LocalContext.current
    Toast.makeText(context, message, duration).show()
}